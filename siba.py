# This is a sample Python script.
import requests
from bs4 import BeautifulSoup
import re
import time
# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
path_w = "d:/AI/ラーメン/siba.txt"
def Scraping(URL):
    html = requests.get(URL)
    html = BeautifulSoup(html.content, "html.parser")
    return html
def get_list(bs,return_list): # requestsでとったそれぞれの要素のテキストを出力?
    # print(bs)
    for name in bs:
        # print(name.text)
        # tmp = name.text.replace('\n', '')
        # tmp = tmp.replace(' ', '')
        return_list.append(name.text)
def get_list_URL(bs,return_list): #
    # print(bs)
    for name in bs:
        # print(name.text)
        # tmp = name.text.replace('\n', '')
        # tmp = tmp.replace(' ', '')
        tmp = str(name.get('href'))
        tmp = tmp.replace("//","")
        return_list.append(tmp)
def get_list_Name (bs,return_list): #aタグの文字列を出力
    # print(bs)
    for name in bs:
        # print(name.text)
        # tmp = name.text.replace('\n', '')
        # tmp = tmp.replace(' ', '')
        print(name.text)
        tmp = str(name.text)
        return_list.append(tmp)
def replace(names):
    res = re.sub(r'<.*>"',"",str(names))
    return res
def filewrite(moji):
    with open(path_w, mode='a',encoding='utf-8') as f:
       f.write(moji)
# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    page_num  = 1 # 存在するページ数
    while True:
        page_URL = 'https://www.jalan.net/gourmet/050000/g2_3g130/page_'+str(page_num)+'/?screenId=OUW1701&influxKbn=0'
        status_code = requests.get(page_URL).status_code
        print('処理中',page_num)
        time.sleep(0.5)
        print(status_code)
        if status_code != 200:
            page_num -= 1
            break
        page_num += 1
    for current_page in range(1,page_num+1): # ページ数分。メインループ
        URL = []
        name = []
        current_page_URL = 'https://www.jalan.net/gourmet/050000/g2_3g130/page_'+str(current_page)+'/?screenId=OUW1701&influxKbn=0'
        html = Scraping(current_page_URL)
        shop_names = html.find_all("a", href=re.compile("//www.jalan.net/gourmet/grm.*/"))
        #URLと店名をそれぞれ配列に格納する。
        get_list_URL(shop_names, URL)
        get_list_Name(shop_names, name)
        ######nameリストの現在のインデックスに口コミという文字が含まれていたら改行し尚且つ次のインデックスに口コミという文字が含まれていなかったら改行。
        URL_index = 0 # URLリストのインデックス
        for name_num in range(len(name)+1): #現在のページの店名と件数分でループ
            try:
                shopname = name[name_num]
                print(shopname + "\n")
                print(name[name_num],name[name_num+1])
                
            except:
                print("over")
                filewrite(shopname)
                break
            if "口コミ" not in name[name_num+1]:# 次が口コミ件数ではないとき
                if "口コミ" in name[name_num]:  # 今口コミ件数ではないとき改行
                    shopname = shopname + "," + URL[URL_index] + ","
                    URL_index += 1
                    shopname = shopname + URL[URL_index] + ",\n"
                    URL_index += 1
                else:
                    print(name_num)
                    shopname = shopname + "," + URL[URL_index] + ",\n"
                    URL_index += 1
            else:  # 名前
                shopname = shopname + ","
        
            filewrite(shopname)
# See PyCharm help at https://www.jetbrains.com/help/pycharm/