import requests
from bs4 import BeautifulSoup
import time
import random
import csv
import os

# from urllib.parse import urljoin
curent = 'd:/AI/ラーメン/'
path_w = "d:/AI/ラーメン/siba.csv"
mainURL = 'https://www.jalan.net/gourmet/050000/g2_3g130/'
r = []
s = []
all_rating= [] #全ての店舗の評価を保存している配列
detail_rating= [] #店舗ごとの評価を一時的に保管している配列

# url = 'https://www.jalan.net/gourmet/050000/g2_3g130/'

# def Scraping(URL):
#     html = requests.get(URL)
#     html = BeautifulSoup(html.content, "html.parser")
#     return html
# with open(path_w, mode='w',encoding='utf-8') as f:
#     f.write()
# print(Scraping(url))

# This is a sample Python script.
import requests
from bs4 import BeautifulSoup
import re
# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
def Scraping(URL):
    html = requests.get(URL)
    html = BeautifulSoup(html.content, "html.parser")
    return html
def get_list(bs,return_list):
    # print(bs)
    for name in bs:
        # print(name.text)
        # tmp = name.text.replace('\n', '')
        # tmp = tmp.replace(' ', '')
        return_list.append(name.text)
def get_list_URL(bs,return_list):
    # print(bs)
    for name in bs:
        # print(name.text)
        # tmp = name.text.replace('\n', '')
        # tmp = tmp.replace(' ', '')
        tmp = str(name.get('href'))
        tmp = tmp.replace("//","")
        return_list.append(tmp)
def get_list_NAME(bs,return_list):
    # print(bs)
    for name in bs:
        # print(name.text)
        # tmp = name.text.replace('\n', '')
        # tmp = tmp.replace(' ', '')
        tmp = str(name.get_text('a'))
        # tmp = str(tmp.get_text())
        tmp = tmp.replace("//","")
        return_list.append(tmp)

def replace(names):
    res = re.sub(r'<.*>"',"",str(names))
    return res

def filewrite(moji,path):

    if path==1:
        with open(path_w, mode='a',encoding='utf-8') as f:
            f.write(moji)  

    elif path==2:
        with open(path, mode='a',encoding='utf-8') as f:
            f.write(moji)
    else:
        with open(path, mode='a',encoding='utf-8') as f:
            f.write(moji) 
    

def shopname_main(pagenum):
    subURL = mainURL+'page_'+str(pagenum)+'/'
    print(subURL)
    html = Scraping(subURL)
    
    shop_names = html.find_all("a", href=re.compile("//www.jalan.net/gourmet/grm_.*"))
    get_list_URL(shop_names,r)
    print(r)
    get_list_NAME(shop_names,s)
    shop_names = replace(shop_names)
    print(s)
    

    test = 0
    shopnum = 1
    
    for shopname in s:
        if "口コミ" in shopname : #口コミ数で改行
            shopname = shopname + "," + r[test]+","
            test += 1
            shopname = shopname + r[test]+",\n"
            test += 1
            # print("口コミ",len(s))
            
        else: #名前
            shopname = shopname +","
            try:
                if "口コミ" not in s[shopnum]:
                    shopname = shopname + "\n"
                    shopnum += 1
            except:
                pass
        filewrite(shopname,1)

def timersleep():
    sec = random.uniform(1, 3)
    time.sleep(sec)

def csv_read(path):# 全ての店舗の配列を配列に入れる。
    abc=[]
    with open(path, encoding="utf-8") as f:
        reader = csv.reader(f)
        for line in reader:
            abc.append(line)
        return abc
    
def csv_read_url(csv_list):# csv_readで作った配列からURLのみを抜き出す。
    urls = []
    for shop in csv_list:
        urls.append(shop[2])
    return urls

# Press the green button in the gutter to run the script.
if __name__ == '__main__':


    # 一時的に１ページしかとらないようにしている。
    # 作成後は下５行コメント化
    with open(path_w, mode='w',encoding='utf-8') as f:
        f.write('')
    for num in range(1,2):
        shopname_main(num)
        timersleep()

    # print("test")
    # print(os.path.exists("./siba.csv"))
    # csv_file = open("./siba.csv", "r")
    # ffaf = csv.reader(csv_file, delimiter=",", doublequote=True, lineterminator="\r\n", quotechar='"', skipinitialspace=True)
    # with open('./siba.csv', encoding="utf-8") as f:
    #     reader = csv.reader(f)
    #     for line in reader:
    #         print(line)

    shop_csv = csv_read("./siba.csv")
    print(shop_csv)
    shopurl_csv = csv_read_url(shop_csv)
    print(shopurl_csv)

    
    
    for shop_url in shopurl_csv :# 店舗詳細を見るループ
        shopdetail = Scraping("https://"+shop_url) 
        print("https://"+shop_url)
        shopdetail_rating =shopdetail.find_all("div", class_='galleryArea-galleryGraph')
        # print(shopdetail_rating)
        for test in shopdetail_rating:# そのページのdivタグのgalleryArea-galleryGraphの中身のタグをまとめた配列みたいなやつtestにいれる
            print("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"")
            keep = test.find_all(class_="reviewPoint")#reviewPointをkeepに。
            
            for review in keep:
                print(review.text) # その店の評価をすべて取得
                temp =str(review.text)
                detail_rating.append(temp)    

            if len(detail_rating) < 10:
                plus_num = 10 - len(detail_rating)
                print(plus_num,len(detail_rating))
                for numm in range(plus_num):
                    detail_rating.append('-,-')          

            print(detail_rating,"c")
            all_rating.append(detail_rating)

# 評価がそろわないのはbsでタグから取ってる来るときにないからずれる。
# 配列の中身をそろえるのが目標

        detail_rating = []
    print(all_rating)
    print(len(all_rating))
    print("----------------------------------------")    
        # for hyoka in g:
        #     print(str(hyoka)+"\n\n\n\n\n\n\n\n\n")
        #     k = hyoka.find(class_="reviewPoint")
        #     k = k.text
        #     print(k)
        #     k = k.replace('\t','').replace('\n','')
        #     hyoukabunnpu.append(k)
        # ditallbunnpu.append(hyoukabunnpu)
        # hyoukabunnpu = []
        # print(ditallbunnpu)
        # print(hyoukabunnpu)
            
   
    # shopname = html5.find('h1',class_='basicTitle').get_text()
    # hyouka = []
    # span=html5.find_all('span',class_='galleryArea-percentage')
    # for i in span:
    #     hyouka.append(i.text)
    # print(hyouka)
    # # print(good[1])
    # filewrite(str(html5),curent+'dall.txt')
    # print(ditallurl)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/